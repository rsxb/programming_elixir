defmodule ListOps do
  @spec map([any], (any -> any)) :: [any]
  def map([], _fun), do: []
  def map([head | tail], fun), do: [fun.(head) | map(tail, fun)]

  @spec reduce([any], any, (any, any -> any)) :: any
  def reduce([], acc, _fun), do: acc

  def reduce([head | tail], acc, fun) do
    reduce(tail, fun.(head, acc), fun)
  end

  @spec mapsum([number], number, (number -> number)) :: number
  def mapsum(list, value, fun) do
    list
    |> map(fun)
    |> reduce(value, fn x, acc -> acc + x end)
  end

  @spec max([number]) :: number
  def max([head | tail]), do: reduce(tail, head, &do_max/2)

  defp do_max(x, acc) when x > acc, do: x
  defp do_max(_, acc), do: acc

  @spec span(number, number) :: [number]
  def span(from, to)
  def span(last, last), do: [last]

  def span(from, to) when to > from do
    [from | span(from + 1, to)]
  end

  @spec all?([any], (any -> bool)) :: boolean
  def all?(list, fun), do: do_all(list, fun, true)

  defp do_all(_, _, false), do: false
  defp do_all([], _fun, _), do: true
  defp do_all([head | tail], fun, _), do: do_all(tail, fun, fun.(head))

  @spec each([any], (any -> any)) :: :ok
  def each([], _fun), do: :ok

  def each([head | tail], fun) do
    fun.(head)
    each(tail, fun)
  end

  @spec filter([any], (any -> bool)) :: [any]
  def filter([], _fun), do: []

  def filter([head | tail], fun) do
    if fun.(head) do
      [head | filter(tail, fun)]
    else
      filter(tail, fun)
    end
  end

  @spec split([any], integer) :: {[any], [any]}
  def split(list, count), do: do_split(list, count, [])

  defp do_split(list, 0, acc), do: {reverse(acc), list}
  defp do_split([], _, acc), do: {reverse(acc), []}

  defp do_split([head | tail], count, acc) do
    do_split(tail, count - 1, [head | acc])
  end

  @spec take([any], integer) :: [any]
  def take(list, amount), do: do_take(list, amount, [])

  defp do_take(_list, 0, acc), do: reverse(acc)
  defp do_take([], _, acc), do: reverse(acc)

  defp do_take([head | tail], amount, acc) do
    do_take(tail, amount - 1, [head | acc])
  end

  @spec flatten([[any], ...]) :: [any]
  def flatten([]), do: []
  def flatten(list), do: do_flatten(list, [])

  defp do_flatten([head | tail], acc) when is_list(head),
    do: do_flatten(head, do_flatten(tail, acc))

  defp do_flatten([head | tail], acc), do: [head | do_flatten(tail, acc)]
  defp do_flatten([], acc), do: acc

  defp reverse(list), do: reverse(list, [])
  defp reverse([], acc), do: acc
  defp reverse([head | tail], acc), do: reverse(tail, [head | acc])
end
